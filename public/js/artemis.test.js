/*global global, ARTEMIS, Worker */
(function (global, ARTEMIS) {
  'use strict';

  function Test() {
    this.currentTest = -1;
  };

  Test.prototype = Object.assign({}, clone(ARTEMIS.Observ), {
    worker: null,
    abandonId: null,
    tests: [],
    currentTestCase: {},
    currentCode: '',
    currentTest: -1,
    set: function (tests) {
      this.currentTest = -1;
      this.tests = tests || [];
      return this;
    },
    get: function () {
      return this.tests;
    },
    start: function (tests) {
      this.set(tests).nextTest().setupWorker();
    },
    runTests: function (code) {
      this.currentCases = this.tests[this.currentTest].cases.slice(0);
      this.currentCode = code;
      this.execute();
      return this;
    },
    execute: function () {
      var functionCall;

      if (this.currentCases.length > 0) {
        this.currentTestCase = this.currentCases.shift();
        functionCall = this.tests[this.currentTest].functionName + '(' + this.currentTestCase['in'] + ')';
        this.dispatch('test.run', { functionCall: functionCall });
        try {
          this.worker.postMessage(this.currentCode + '\n' + functionCall);
          this.abandonId = global.setTimeout(this.abandon.bind(this), 500);
        } catch (e) {
          this.dispatch('test.run.failed');
          this.resume(this.currentTest);
        }

        return this;
      }
      this.complete(this.currentTest); //all the unit tests for this questions are done

      if (this.currentTest === this.tests.length - 1) {
        this.done(); // all the tests are done
      } else {
        this.next(); // there are still tests available
      }
      return this;
    },
    validate: function (result) {
      global.clearTimeout(this.abandonId);

      if (this.currentTestCase.exp === result) {
        this.dispatch('test.right', { result: result });
        global.setTimeout(this.execute.bind(this), 1000);
        return;
      }
      this.dispatch('test.wrong', { result: result, expected: this.currentTestCase.exp});
      this.resume(this.currentTest);
    },
    error: function (error) {
      global.clearTimeout(this.abandonId);
      this.dispatch('test.error', { error: error });
      this.resume(this.currentTest);
    },
    setupWorker: function () {
      var testWorker = new Worker('js/eval-worker.js');

      testWorker.onmessage = function (msg) {
        var result = JSON.parse(msg.data);
        switch (result.type) {
          case 'result':
            this.validate(result.content);
            break;
          case 'error':
            this.error(result.content);
            break;
          case 'log':
            this.dispatch('test.log', { result: result.content });
            break;
        }
      }.bind(this);

      testWorker.onerror = function (err) {
        this.error(err);
      }.bind(this);

      this.worker = testWorker;
      return this;
    },
    abandon: function () {
      this.worker.terminate();
      this.setupWorker();
      this.dispatch('test.worker.abandon');
    },
    nextTest: function () {
      this.currentTest += 1;
      this.dispatch('test.start', { initial: this.tests[this.currentTest].initial, intro: this.tests[this.currentTest].intro });
      return this;
    },
    next: function () {
      this.dispatch('test.next');
    },
    resume: function (currentTest) {
      this.dispatch('test.resume', { currentTest: currentTest })
    },
    done: function () {
      this.dispatch('test.done');
    },
    complete: function (currentTest) {
      this.dispatch('test.complete', { currentTest: currentTest, outtro:  this.tests[this.currentTest].outtro});
    }
  });

  ARTEMIS.Test = Test;

}(this, this.ARTEMIS || {}));