/*global window, ARTEMIS */
(function (window, ARTEMIS) {
  "use strict";
  
  /**
   * ARTEMIS Clock singleton
   * @param {number} indicator The html element used for rendering
   * @param {number} tickInterval
   */
  ARTEMIS.clock = function (indicator, tickInterval) {
    this.tickInterval = tickInterval || ARTEMIS.clock.TICK_INTERVAL;
    this.indicator = indicator;
    this.timer = new Date(0);
  };
  
  ARTEMIS.clock.prototype = {
    /**
     * saves current time offset, restarts the timeout and cals display
     */
    tick: function () {
      this.timer.setTime(this.timer.getTime() + this.tickInterval);
      this.display();
      this.start();
    },
    /**
     * Render function for clock
     */
    display: function () {
      var currentTime = this.value();
      this.indicator.innerHTML = currentTime;
    },
    /**
     * Returns the formated value of the current time
     */
    value: function () {
      var min, sec;
      min = this.timer.getMinutes();
      sec = this.timer.getSeconds();
      return (min < 10 ? "0" : "") + min + ":" + (sec < 10 ? "0" : "") + sec;
    },
    /**
     * Returns the raw, unformatted value of the current time
     */
    rawValue: function () {
      var min, sec;
      min = this.timer.getMinutes();
      sec = this.timer.getSeconds();

      return parseInt((min ? min * 60 : 0) + sec, 10);
    },
    /**
     * Starts the stopwatch
     */
    start: function () {
      this.timeoutId = window.setTimeout(this.tick.bind(this), this.tickInterval);
    },
    /**
     * Stops the stopwatch
     */
    stop: function () {
      window.clearTimeout(this.timeoutId);
    },
    reset: function () {
      window.clearTimeout(this.timeoutId);
      this.timer = new Date(0);
      this.display();
    },
    timeoutId: null,
    timer: null
  };
  /**
   * default tick interval set to 1000
   */
  ARTEMIS.clock.TICK_INTERVAL = 1000;
}(this, this.ARTEMIS || {}));