describe('ARTEMIS Observ', function () {
    var logger = ARTEMIS.logger(),
        obs = ARTEMIS.Observ, noop = function () { };

    beforeEach(function () {
        obs.clear();
    });

    it('should be possible to register a listener', function () {
        var unsub = obs.subscribe('test', noop);
        expect(obs._listeners).toEqual({
            'test': [{
                fn: noop,
                bind: null
            }]
        });
    });

    it('should be possible to remove listener', function () {
        var unsub = obs.subscribe('test', noop);
        unsub();
        expect(obs._listeners).toEqual({ 'test': [] });
    });

    it('should dispatch an event', function () {
        var spy = jasmine.createSpy();

        var unsub = obs.subscribe('test', spy);
        obs.dispatch('test');
        expect(spy).toHaveBeenCalled();
        unsub();
    });

    it('should dispatch an event and pass params', function () {
        var spy = jasmine.createSpy();

        var unsub = obs.subscribe('test', spy);
        obs.dispatch('test', { foo: 12 });
        expect(spy).toHaveBeenCalledWith({ foo: 12 });
        unsub();
    });

     it('should set the correct context', function () {
        var spy = jasmine.createSpy();
        var obj = { foo: 12 };
        var unsub = obs.subscribe('test', spy, obj);

        obs.dispatch('test');
        var calls = spy.calls.all();
        expect(calls[0].object).toEqual(obj); 
        unsub();
     });

     it('should dispatch an event to all subscribed listeners', function () {
        var spy = jasmine.createSpy();
        var unsub = obs.subscribe('test', spy);
        
        obs.subscribe('test', spy);
        obs.dispatch('test');
        expect(spy.calls.count()).toEqual(2);
        unsub();
     });
});