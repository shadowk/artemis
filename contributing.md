# Contributing to ARTEMIS

Thanks for your interest in improving ARTEMIS! This project welcomes contributions of all kinds: from discussion to documentation to bugfixes to feature improvements, to new tests for the interviews

In depth description of how tests can be added can be found under in the [Guide to creating tests](tests.md)

Please review this document to help to streamline the process and save everyone's precious time.


# Guiding principle 
The project does not use any front-end framework. The aim of this code is to use as much vanilla javascript as possible, thus any and all depenendencies should be avoided if a vanilla alternative is available. 

All dependencies should be treated with a high level of mistrust and one should always ask: *do I really need this or am I just being lazy*?

# Branching
ARTEMIS, follows does not follow the git flow process for branching thus please follow the rules outlined bellow

* ``master`` is always the release that is being worked on next
* ``release`` branches contain the currently active version of the application
     * hotfixes should start from ``release`` and be merged back into ``release`` and ``master``
     * hotfixes are named ``hotfix/{ISSUE-NUMBER}``
* Features for the release that is being worked on next follow the pattern:
    * ``feature/{ISSUE-NUMBER}`` 
    * branches start from ``master`` and get merged back into ``master``
* Bugfixes for the release that is being worked on next follow the pattern:
    * ``bugfix/{ISSUE-NUMBER}`` 
    * branches start from ``master`` and get merged back into ``master``

# Issues

No software is bug free. So, if you got an issue, follow these steps:

* Search the issue list for current and old issues.
    * If you find an existing issue, please UPVOTE the issue by adding a "thumbs-up reaction". I use this to help prioritize issues!
* If none of that is helping, create an issue with with following information:
    * Clear title (make is shorter if possible).
    * Describe the issue in clear language.
    * Share error logs, screenshots and etc.
* To speed up the issue fixing process, send us a sample repo with the issue you faced:

## Testing against ```master```
To test your project  against the current latest version, you can clone the repository and start it by running

```
git clone https://shadowk@bitbucket.org/shadowk/artemis.git
npm install
npm test
node app
```

# Pull Requests (PRs)

I welcome your contributions. There are many ways you can help us. This is few of those ways:

* Fix typos and add more documentation.
* Try to fix some bugs.
* Add more tests (for the canidate).
* Add more unit tests
* Before you submit a new PR, make you to run ```npm test```. Do not submit a PR if tests are failing. If you need any help, create an issue and ask.


## Reviewing PRs

**As a PR submitter**, you should reference the issue if there is one, include a short description of what you contributed and, if it is a code change, instructions for how to manually test out the change. If your PR is reviewed as only needing trivial changes (e.g. small typos etc), and you have commit access, then you can merge the PR after making those changes.

**As a PR reviewer**, you should read through the changes and comment on any potential problems. If you see something cool, a kind word never hurts either! Additionally, you should follow the testing instructions and manually test the changes. If the instructions are missing, unclear, or overly complex, feel free to request better instructions from the submitter. Unless the PR is tagged with the ```do not merge``` label, if you approve the review and there is no other required discussion or changes, you should also go ahead and merge the PR.

# Issue Triage

If you are looking for a way to help the project, triaging issues is a great place to start. Here's how you can help:

## Responding to issues

Issues that are tagged ```question / support``` or needs reproduction are great places to help. If you can answer a question, it will help the asker as well as anyone searching. If an issue needs reproduction, you may be able to guide the reporter toward one, or even reproduce it yourself using this technique.

## Triaging issues

Once you've helped out on a few issues, if you'd like triage access you can help label issues and respond to reporters.

We use the following label scheme to categorize issues:

* **type** - bug, feature, question / support, discussion, maintenance.
* **status** - needs reproduction, needs PR, in progress, etc.

All issues should have a type label. ```bug/feature/question/discussion``` are self-explanatory. ```maintenance``` is a catch-all for any kind of cleanup or refactoring.

If an issue is a ```bug```, and it doesn't have a clear reproduction that you have personally confirmed, label it ```needs reproduction``` and ask the author to try and create a reproduction, or have a go yourself.

## Closing issues

* Duplicate issues should be closed with a link to the original.
* Unreproducible issues should be closed if it's not possible to reproduce them (if the reporter drops offline, it is reasonable to wait 2 weeks before closing).
* ```bug```s should be labelled ```merged``` when merged, and be closed when the issue is fixed and released.
* ```features, maintenances``` should be labelled ```merged``` when merged, and closed when released or if the feature is deemed not appropriate.
* ```question / supports``` should be closed when the question has been answered. If the questioner drops offline, a reasonable period to wait is two weeks.
* ```discussions``` should be closed at a maintainer's discretion.